﻿using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
namespace DoutorIE
{
    /// <summary>
    /// Interaction logic for BluetoothWindow.xaml
    /// </summary>
    public partial class BluetoothWindow : Window
    {
        public BluetoothWindow()
        {
            InitializeComponent();
            if (!BluetoothRadio.IsSupported)
            {
                MessageBox.Show("No Bluetooth Adapter found!!!");
            }
            else
            {
                if (BluetoothRadio.PrimaryRadio.Mode == RadioMode.PowerOff)
                {
                    BluetoothRadio.PrimaryRadio.Mode = RadioMode.Connectable;
                    getListOfDevice();
                    rdbbluetooth.IsChecked = true;
                }
                else if (BluetoothRadio.PrimaryRadio.Mode == RadioMode.Discoverable)
                {
                    rdbbluetooth.IsChecked = true;
                    getListOfDevice();
                }
                else if (BluetoothRadio.PrimaryRadio.Mode == RadioMode.Connectable)
                {
                    rdbbluetooth.IsChecked = true;
                    getListOfDevice();
                }

            }



        }
        public void getListOfDevice()
        {
            List<BluetoothModal> items = new List<BluetoothModal>();
            BluetoothClient client = new BluetoothClient();
            BluetoothDeviceInfo[] devices = client.DiscoverDevices();
            foreach (BluetoothDeviceInfo d in devices)
            {
                items.Add(new BluetoothModal
                {
                    bluetoothid = d.DeviceAddress,
                    name = d.DeviceName
                });

            }
            lstbluetoothdevice.DisplayMemberPath = "name";
            lstbluetoothdevice.SelectedValue = "bluetoothid";
            lstbluetoothdevice.ItemsSource = items;
        }
        public class BluetoothModal
        {
            public BluetoothAddress bluetoothid { get; set; }
            public string name { get; set; }
        }


        private async void lstbluetoothdevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool isPairdDevice = false;
            BluetoothModal bluetoothModal = lstbluetoothdevice.SelectedItem as BluetoothModal;
            //var bluetoothid = bluetoothModal.bluetoothid;
            BluetoothAddress bluetoothAddress = (BluetoothAddress)bluetoothModal.bluetoothid;
            BluetoothClient client = new BluetoothClient();
            //BluetoothDeviceInfo[] devices = client.DiscoverDevices();
            var device = new BluetoothDeviceInfo(bluetoothAddress);
            if (bluetoothAddress == device.DeviceAddress)
            {
                if (device.Authenticated)
                {
                    isPairdDevice = true;
                }
                else
                {
                    isPairdDevice = false;
                }
            }
            //foreach (BluetoothDeviceInfo d in devices)
            //{
            //    if(bluetoothAddress==d.DeviceAddress)
            //    {
            //        if(d.Connected)
            //        {
            //            isPairdDevice = true;
            //            break;
            //        }
            //    }

            //}
            if (!isPairdDevice)
            {
                if (MessageBox.Show(String.Format("Would you like to attempt to pair with {0}?", bluetoothModal.name), "Pair Device", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Guid serviceClass;
                    serviceClass = BluetoothService.RFCommProtocol;
                    if (BluetoothSecurity.PairRequest(bluetoothAddress, "123456"))
                    {
                        MessageBox.Show("We paired!");
                        await Send(bluetoothAddress, "test");
                    }
                    else
                    {
                        MessageBox.Show("Failed to pair!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Device Is Already Paird.");
                await Send(bluetoothAddress, "test");
            }
        }

        public async Task<bool> Send(BluetoothAddress device, string content)
        {
            Guid serviceClass;
            serviceClass = BluetoothService.RFCommProtocol;
            if (device == null)
            {
                throw new ArgumentNullException("device");
            }

            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentNullException("content");
            }

            // for not block the UI it will run in a different threat  
            var task = Task.Run(() =>
            {
                using (var bluetoothClient = new BluetoothClient())
                {
                    try
                    {
                        var ep = new BluetoothEndPoint(device, serviceClass);

                        // connecting  
                        bluetoothClient.Connect(ep);

                        // get stream for send the data  
                        var bluetoothStream = bluetoothClient.GetStream();

                        // if all is ok to send  
                        if (bluetoothClient.Connected && bluetoothStream != null)
                        {
                            // write the data in the stream  
                            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                            bluetoothStream.Write(buffer, 0, buffer.Length);
                            bluetoothStream.Flush();
                            bluetoothStream.Close();
                            return true;
                        }
                        return false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        // the error will be ignored and the send data will report as not sent  
                        // for understood the type of the error, handle the exception  
                    }
                }
                return false;
            });
            return await task;
        }

    }
}

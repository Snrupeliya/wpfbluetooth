﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace DoutorIE
{
    public class App : System.Windows.Application
    {
        [STAThread]
        public static void Main()
        {
            App app = new App();
            if (RedistributablePackage.IsInstalled(RedistributablePackageVersion.VC2015to2019x64))
            {
                try
                {
                    app.StartupUri = new System.Uri("StartPage.xaml", System.UriKind.Relative);
                    app.Run();
                }
                catch 
                {

                    
                }
               
            }
            else
            {
                if (MessageBox.Show(
                    "O componente Microsoft Visual C++ 2015 ou superior é necessário para executar o aplicativo Doutor-IE. Deseja instalá-lo?",
                    "Microsoft Visual C++"
                    
                    , MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    System.Diagnostics.Process.Start("https://drieonline.com/visualc.php");
                    System.Threading.Thread.Sleep(2000);
                    System.Environment.Exit(0);
                    // Close the window  
                }
                else
                {
                    System.Environment.Exit(0);
                    // Do not close the window  
                }
            }
        }
    }
}

﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.Wpf;

using System;
using System.ComponentModel;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace DoutorIE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {//rodrigorreis
        public static string version = "1.0.16";
        public static string HelloMessage = "Hello World xxxx";
        public string MacAddress { get { return HelloMessage; } }
        CefSharp.Wpf.ChromiumWebBrowser chromium;
        private Uri currentUri;
        public static MainWindow AppWindow;
        public static SplashScreen splash;
        public static string subscribermsg = "rodrigorreis";
        public string subscriber { get; }

        public static string theScript = @"
                   function getLocation() {
                          if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition);
                          } else { 
                           alert('Geolocation is not supported by this browser.');
                          }
                        }

                    function showPosition(position)
                    {
                        alert('Latitude: ' + position.coords.latitude +
                        '<br>Longitude: ' + position.coords.longitude;
                    } ";


        public MainWindow()
        {

            var userAgentParams = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.098 Safari/537.36";
            splash = new SplashScreen();
            splash.Show();
            CefSettings settings = new CefSettings();
            settings.Locale = "pt-BR";
            settings.AcceptLanguageList = "pt-BR,en-US";
            settings.CachePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CEF";
            settings.UserAgent = userAgentParams;
            //Cef //Delete all cookies.
            CefSharp.Cef.Initialize(settings);
            InitializeComponent();

            // get geolocation for the native app
            //var detailsFromClass = GeoLocationDetails.GetMyGeoLocation();
            //MessageBox.Show(detailsFromClass, "Geo Location", MessageBoxButton.OK);


            chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser");
            cefSharpWebBrowser.Address = "https://drieonline.com/access.php?type=desktop";
            chromium.JavascriptObjectRepository.Register("boundAsync", new BoundObject(), true);
            chromium.JavascriptObjectRepository.Register("boundSubAsync", new BoundSubscribers(), true);


            var data = chromium.Address;
            btnAlert.Visibility = Visibility.Hidden;
            chromium.LoadingStateChanged += BrowserLoadingStateChanged;
            AppWindow = this;
            //AppWindow.WindowState = WindowState.Minimized;
            //  chromium.RequestHandler = new CustomRequestHandler("");
            chromium.DownloadHandler = new DownloadHandler(Bar);
            chromium.LoadError += OnLoadError;

            
            
           






        }
        private void OnLoadError(object sender, LoadErrorEventArgs args)
        {
            if (args.ErrorCode == CefSharp.CefErrorCode.InternetDisconnected)
            {
                // System.Threading.Thread.Sleep(2000);
                //MessageBox.Show("Please check your internet connection.");
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                    }));
                }
            }
            else
            {
                //if (Application.Current.Dispatcher.CheckAccess())
                //{
                //    MessageBox.Show(Application.Current.MainWindow,args.ErrorText);
                //}
                //else
                //{
                //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                //        MessageBox.Show(Application.Current.MainWindow, args.ErrorText);
                //    }));
                //}
            }
        }

        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            try
            {
                if (splash.IsVisible)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        AppWindow.Hide();
                        AppWindow.WindowState = WindowState.Maximized;
                        AppWindow.chromium.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
                        AppWindow.chromium.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
                    });
                }
                if (!e.IsLoading)
                {
                    if (e.Browser.MainFrame.Url == "https://drieonline.com/temp_integration/alert_js.html")
                    {
                        Application.Current.Dispatcher.Invoke(() => { AppWindow.btnAlert.Visibility = Visibility.Visible; });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() => { AppWindow.btnAlert.Visibility = Visibility.Hidden; });
                    }
                    if (e.Browser.MainFrame.Url == "https://www.drieonline.com/logout.php")
                    {
                    }
                    Application.Current.Dispatcher.Invoke(() => { splash.Close();
                      var scriptTask=  e.Browser.MainFrame.EvaluateScriptAsync(theScript);
                        scriptTask.ContinueWith(u =>
                        {
                            if (u.Result.Success && u.Result.Result != null)
                            {
                            }
                        });
                           
                    });
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif.Visibility = Visibility.Hidden; });
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.Show(); });
                    // AppWindow.WindowState = WindowState.Maximized;
                    //Application.Current.Dispatcher.Invoke(() => {  });

                    //new Thread(delegate () {
                    //    MessageBox.Show(Convert.ToString(FindGeolocationByIp.GetGeoLocationAsync().Result),"Geo location",MessageBoxButton.OK);
                    //}).Start();
                    
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.btnAlert.Visibility = Visibility.Hidden; });
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif.Visibility = Visibility.Visible; });
                }
            }
            catch
            {
            }
        }
        //private static void Browser2LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        //{
        //    if (!e.IsLoading)
        //    {
        //        Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif2.Visibility = Visibility.Hidden; });
        //    }
        //    else
        //    {
        //       Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif2.Visibility = Visibility.Visible; });
        //    }
        //}
        private async void Alert_Click(object sender, RoutedEventArgs e)
        {
            //var browser = new ChromiumWebBrowser("https://drieonline.com/temp_integration/alert_js.html");

            if (chromium.CanExecuteJavascriptInMainFrame)
            {
                chromium.GetMainFrame().ExecuteJavaScriptAsync(@"someJQFunction();");
                //JavascriptResponse response = await browser.EvaluateScriptAsync("d");
                //if (response.Result != null)
                //{
                //    //MessageBox.Show(response.Result.ToString, "JS Result");
                //}
            }
        }

        /// <summary>
        /// Generate alert from the browser
        /// </summary>
        public void alertMessage()
        {
            //chromium.EvaluateScriptAsync
            chromium.GetMainFrame().ExecuteJavaScriptAsync(@"someJQFunction();");
        }



        public static string GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    HelloMessage = Convert.ToString(nic.GetPhysicalAddress());
                    return nic.GetPhysicalAddress().ToString();
                }
            }
            return null;
        }

        private void btnbluetooth_Click(object sender, RoutedEventArgs e)
        {
            var bluetoothpage = new BluetoothWindow();
            bluetoothpage.Show();
            this.Close();
        }

        private void mytab2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //mytab2.Content = Application.LoadComponent(new Uri("UserControl1.xaml", UriKind.Relative));
        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

        //    switch (tabItem)
        //    {
        //        case "Home2":
        //            if (chromium != null)
        //            {

        //                chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser2");
        //                chromium.LoadingStateChanged += Browser2LoadingStateChanged;
        //                chromium.LoadError += OnLoadError;
        //            }
        //            break;

        //        case "Bluetooth":

        //            break;

        //        case "Home":

        //           if(chromium !=null)
        //            {
        //             chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser");

        //            }
        //            break;

        //        default:
        //            return;
        //    }
        //}

        protected async override void OnClosing(CancelEventArgs e)
        {

            MessageBoxResult result = CustomMessageBox.ShowYesNo(
                "Deseja realmente sair do aplicativo Doutor-IE?",
                "Doutor-IE",
                "Sim",
                "Não",
                MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.Yes)
            {
                e.Cancel = true;
                //MessageBox.Show("Encerrando o aplicativo, por gentileza aguarde...", "Message Box");
                try
                {
                    chromium.ExecuteScriptAsync("location.href = 'logout.php';");
                    await cefSharpWebBrowser.GetSourceAsync().ContinueWith(taskHtml =>
                      {
                          var html = taskHtml.Result;
                          cefSharpWebBrowser.GetCookieManager().DeleteCookies();
                          System.Threading.Thread.Sleep(2000);
                          this.Dispatcher.Invoke(() =>
                          {
                              System.Environment.Exit(0);
                          });
                      });
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Encerrando o aplicativo, por gentileza aguarde...", "Message Box");
                }
            }

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }



        private void menu_MouseDown(object sender, MouseEventArgs e)
        {
            if (lblMenuContent.IsVisible)
            {
                lblMenuContent.Visibility = Visibility.Hidden;
            }
            else
            {
                lblMenuContent.Visibility = Visibility.Visible;
            }
        }



        //public string Encode(string input, byte[] key)
        //{
        //    HMACSHA1 myhmacsha1 = new HMACSHA1(key);
        //    byte[] byteArray = Encoding.ASCII.GetBytes(input);
        //    MemoryStream stream = new MemoryStream(byteArray);
        //    return myhmacsha1.ComputeHash(stream).Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);
        //}

        //private string CreateToken(string message, string secret)
        //{
        //    secret = secret ?? "";
        //    var encoding = new System.Text.ASCIIEncoding();
        //    byte[] keyByte = encoding.GetBytes(secret);
        //    byte[] messageBytes = encoding.GetBytes(message);
        //    using (var hmacsha1 = new HMACSHA1(keyByte))
        //    {
        //        byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
        //        return Convert.ToBase64String(hashmessage);
        //    }
        //}

        private void Browser_PreviewMouseRightButton(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void Button_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void BackButtonClick(object sender, EventArgs e)
        {
            chromium.Back();
        }

        private void ForwardButtonClick(object sender, EventArgs e)
        {
            chromium.Forward();
        }
        private void ReloadButtonClick(object sender, EventArgs e)
        {
            chromium.Reload();
        }

        private void OpenContactMenu(object sender, MouseButtonEventArgs e)
        {

            Image image = sender as Image;
            ContextMenu contextMenu = image.ContextMenu;
            contextMenu.PlacementTarget = image;
            contextMenu.IsOpen = true;
            e.Handled = true;

        }
        private void Sobre_Click(object sender, RoutedEventArgs e)
        {
            chromium.ExecuteScriptAsync("getLocation()");

            MessageBox.Show("Doutor-IE Tecnologia Automotiva"
                 + Environment.NewLine +
                "Copyright © 2020 Todos os direitos reservados"
                 + Environment.NewLine + " "
                + Environment.NewLine +
                "Telefone: (48) 3238-0010"
                 + Environment.NewLine +
                 "Site: https://www.doutorie.com.br"
                 + Environment.NewLine + " "
                 + Environment.NewLine +
                 "Versão   " + version + " "
               , "Sobre");


        }




    }

    //public class BoundObject
    //{

    //    public string getMacAddress(string msg)
    //    {
    //        var mac = GetMacAddress();
    //        return mac;
    //    }

    //    public static string GetMacAddress()
    //    {
    //        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
    //        {
    //            // Only consider Ethernet network interfaces
    //            if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
    //                nic.OperationalStatus == OperationalStatus.Up)
    //            {
    //                return nic.GetPhysicalAddress().ToString();
    //            }
    //        }
    //        return null;
    //    }

    //    public string getSignature(string msg)
    //    {
    //        var se = GetSignature();
    //        return se;
    //    }

    //    public static string GetSignature()
    //    {
    //        string signature = null;
    //        if (!string.IsNullOrEmpty(GetSetComanDetails.Signature))
    //        {
    //            var macAddress = GetMacAddress();
    //            if (!string.IsNullOrWhiteSpace(macAddress))
    //            {
    //                signature = GetSetComanDetails.Signature + "," + macAddress;
    //            }
    //            else
    //            {
    //                signature = GetSetComanDetails.Signature;
    //            }

    //        }
    //        return signature;
    //    }
    //}



    //// bind the subscriber and created the signature 
    //public class BoundSubscribers
    //{
    //    public void getSubscribers(string subscriberName, string date)
    //    {
    //        //API key code
    //        byte[] bytes = new byte[16];
    //        var rng = new RNGCryptoServiceProvider();
    //        rng.GetBytes(bytes);
    //        var key = "iiwsivk8ab624sszj6g85y9304ee9wkh";
    //        SiteVariable.Key = key;
    //        var ccDate = DateTime.Now.ToString("dd/MM/yyyy");
    //        var currentDate = ccDate.Replace("-", "/");
    //        var secretkey = subscriberName + "_" + date;
    //        // get signature
    //        var signature = GenerateSignature(secretkey, key); // S = MAC(K, ID, D) where S is signature, K is key, ID is subscriber and D is current data
    //        // set values
    //        GetSetComanDetails.Signature = signature;
    //    }
    //    public string GenerateSignature(string secretKey, string signatureString)
    //    {
    //        var enc = Encoding.ASCII;
    //        HMACSHA1 hmac = new HMACSHA1(enc.GetBytes(secretKey));
    //        hmac.Initialize();
    //        byte[] buffer = enc.GetBytes(signatureString);
    //        return BitConverter.ToString(hmac.ComputeHash(buffer)).Replace("-", "").ToLower();
    //    }

    //}
    //public static class GetSetComanDetails
    //{
    //    public static string Signature { get; set; }
    //}

    //public class DownloadHandler : IDownloadHandler
    //{
    //    private ProgressBar _bar;
    //    public DownloadHandler(ProgressBar bar)
    //    {
    //        _bar = bar;

    //    }
    //    public void OnBeforeDownload(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
    //    {
    //        if (!callback.IsDisposed)
    //        {
    //            using (callback)
    //            {
    //                string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
    //                //string pathDownload = Path.Combine(pathUser, "Downloads\\");
    //                //System.IO.FileInfo file = new System.IO.FileInfo(downloadItem.FullPath);
    //                if (browser.MainFrame.Url == "https://drieonline.com/temp_versao.php")
    //                {
    //                    if (downloadItem.MimeType == "application/csv")
    //                    {
    //                        callback.Continue(pathUser + "\\Downloads\\" + downloadItem.SuggestedFileName, showDialog: false);
    //                    }
    //                    else
    //                    {
    //                        callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
    //                    }
    //                }
    //                else
    //                {
    //                    callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
    //                }


    //            }
    //        }
    //    }

    //    public void OnDownloadUpdated(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
    //    {

    //        _bar.Dispatcher.Invoke(new Action(() =>
    //        {
    //            Debug.Print("{0}/{1} bytes", downloadItem.ReceivedBytes, downloadItem.TotalBytes);
    //            _bar.Visibility = Visibility.Hidden;
    //            //_bar.Maximum = downloadItem.TotalBytes;
    //            _bar.Value = downloadItem.PercentComplete;
    //            if (downloadItem.IsComplete)
    //            {
    //                System.Threading.Thread.Sleep(2000);
    //                _bar.Visibility = Visibility.Hidden;
    //                if (browser.MainFrame.Url == "https://drieonline.com/temp_versao.php")
    //                {
    //                    if (downloadItem.MimeType == "application/csv")
    //                    {

    //                        _bar.Visibility = Visibility.Hidden;
    //                        //MessageBox.Show("File download sccessfully at location: " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\");
    //                        CustomMessageBox.Show(
    //                           "Download realizado com sucesso! at " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\",
    //                           "Doutor-IE");

    //                    }
    //                    else
    //                    {
    //                        _bar.Visibility = Visibility.Hidden;
    //                        CustomMessageBox.Show(
    //                              "Download realizado com sucesso!",
    //                              "Doutor-IE");
    //                        //MessageBox.Show("File download sccessfully.");
    //                    }
    //                }
    //                else
    //                {
    //                    _bar.Visibility = Visibility.Hidden;
    //                    CustomMessageBox.Show(
    //                              "Download realizado com sucesso!",
    //                              "Doutor-IE");
    //                    //MessageBox.Show("File download sccessfully.");
    //                }
    //            }
    //        }));
    //    }
    //}



    //public class CustomResourceRequestHandler : ResourceRequestHandler
    //{
    //    private string _signature;
    //    public CustomResourceRequestHandler(string signature)
    //    {
    //        _signature = GetSetComanDetails.Signature;
    //    }

    //    protected override CefReturnValue OnBeforeResourceLoad(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
    //    {
    //        if (request.Url.Contains("https://drieonline.com"))
    //        {
    //            if (!string.IsNullOrEmpty(GetSetComanDetails.Signature))
    //            {
    //                var headers = request.Headers;
    //                headers["s"] = GetSetComanDetails.Signature;
    //                request.Headers = headers;
    //                return CefReturnValue.Continue;
    //            }
    //            else return base.OnBeforeResourceLoad(chromiumWebBrowser, browser, frame, request, callback);
    //        }
    //        else return base.OnBeforeResourceLoad(chromiumWebBrowser, browser, frame, request, callback);
    //    }

    //}
    //public class CustomRequestHandler : RequestHandler
    //{
    //    private string _signature;
    //    public CustomRequestHandler(string signature)
    //    {
    //        _signature = GetSetComanDetails.Signature;
    //    }

    //    protected override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
    //    {
    //        if (request.Url.Contains("https://drieonline.com") && request.Url.Contains(".php"))
    //        {
    //            if (!string.IsNullOrEmpty(GetSetComanDetails.Signature)) return new CustomResourceRequestHandler(GetSetComanDetails.Signature);
    //            else return base.GetResourceRequestHandler(chromiumWebBrowser, browser, frame, request, isNavigation, isDownload, requestInitiator, ref disableDefaultHandling);
    //        }
    //        else return base.GetResourceRequestHandler(chromiumWebBrowser, browser, frame, request, isNavigation, isDownload, requestInitiator, ref disableDefaultHandling);
    //    }
    //}


}
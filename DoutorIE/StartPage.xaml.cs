﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.Wpf;

using System;
using System.ComponentModel;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace DoutorIE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class StartPage : Window
    {//rodrigorreis
        public static string version = "1.0.16";
        public static string HelloMessage = "Hello World xxxx";
        public string MacAddress { get { return HelloMessage; } }
        CefSharp.Wpf.ChromiumWebBrowser chromium;
        private Uri currentUri;
        public static StartPage AppWindow;
        public static SplashScreen splash;

        System.Timers.Timer timer = null;


        public StartPage()
        {

            var userAgentParams = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.098 Safari/537.36";
            splash = new SplashScreen();
            splash.Show();
            try
            {
                CefSettings settings = new CefSettings();
                settings.Locale = "pt-BR";
                settings.AcceptLanguageList = "pt-BR,en-US";
                settings.CachePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CEF";
                settings.UserAgent = userAgentParams;
                //Cef //Delete all cookies.
                CefSharp.Cef.Initialize(settings);
            }
            catch (Exception ex)
            {

            }

            InitializeComponent();

            string appDir = Path.GetDirectoryName(
             Assembly.GetExecutingAssembly().GetName().CodeBase);
            string myFile = Path.Combine(appDir, "start_page.html");
            Uri fileUrl = new Uri(myFile);
            var strFile = fileUrl.PathAndQuery;


            chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser");
            cefSharpWebBrowser.Address = strFile;

            chromium.JavascriptObjectRepository.Register("boundScreenAsync", new BoundScreenName(), true);

            var data = chromium.Address;

            chromium.LoadingStateChanged += BrowserLoadingStateChanged;
            AppWindow = this;
            chromium.LoadError += OnLoadError;

            timer = new System.Timers.Timer(1000);
            timer.AutoReset = true; // the key is here so it repeats
            timer.Elapsed += timer1_Tick;
            timer.Start();
        }
        private void OnLoadError(object sender, LoadErrorEventArgs args)
        {
            if (args.ErrorCode == CefSharp.CefErrorCode.InternetDisconnected)
            {
                // System.Threading.Thread.Sleep(2000);
                //MessageBox.Show("Please check your internet connection.");
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                    }));
                }
            }
            else
            {
            }
        }

        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            try
            {
                if (splash.IsVisible)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        AppWindow.Hide();
                        AppWindow.WindowState = WindowState.Maximized;
                        AppWindow.chromium.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
                        AppWindow.chromium.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
                    });
                }
                if (!e.IsLoading)
                {
                    if (e.Browser.MainFrame.Url == "https://www.drieonline.com/logout.php")
                    {
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        splash.Close();
                    });
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif.Visibility = Visibility.Hidden; });
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.Show(); });
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif.Visibility = Visibility.Visible; });
                }
            }
            catch
            {
            }
        }

        protected async override void OnClosing(CancelEventArgs e)
        {

            MessageBoxResult result = CustomMessageBox.ShowYesNo(
                "Deseja realmente sair do aplicativo Doutor-IE?",
                "Doutor-IE",
                "Sim",
                "Não",
                MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.Yes)
            {

                this.Dispatcher.Invoke(() =>
                {
                    System.Environment.Exit(0);
                });

            }



            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRedirect = ScreenValue.StrScreenName;
            if (!string.IsNullOrEmpty(strRedirect))
            {
                if (strRedirect == "WebView")
                {
                    ScreenValue.StrScreenName = string.Empty;
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        toggle NewWindow = new toggle(true);
                        NewWindow.Show();
                        AppWindow.Hide();

                    });
                }
                else if (strRedirect == "Bluetooth")
                {
                    ScreenValue.StrScreenName = string.Empty;
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        toggle NewWindow = new toggle(false);
                        NewWindow.Show();
                        AppWindow.Hide();

                    });
                }

            }
            else
            {


            }
        }


        private void OpenContactMenu(object sender, MouseButtonEventArgs e)
        {

            Image image = sender as Image;
            ContextMenu contextMenu = image.ContextMenu;
            contextMenu.PlacementTarget = image;
            contextMenu.IsOpen = true;
            e.Handled = true;

        }
        private void Sobre_Click(object sender, RoutedEventArgs e)
        {
            chromium.ExecuteScriptAsync("getLocation()");

            MessageBox.Show("Doutor-IE Tecnologia Automotiva"
                 + Environment.NewLine +
                "Copyright © 2020 Todos os direitos reservados"
                 + Environment.NewLine + " "
                + Environment.NewLine +
                "Telefone: (48) 3238-0010"
                 + Environment.NewLine +
                 "Site: https://www.doutorie.com.br"
                 + Environment.NewLine + " "
                 + Environment.NewLine +
                 "Versão   " + version + " "
               , "Sobre");


        }


    }
    public class BoundScreenName
    {
        public void getScreenName(string screenName)
        {
            ScreenValue.StrScreenName = screenName;
        }
    }
    public static class ScreenValue
    {

        public static string StrScreenName { get; set; }
    }
}












﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.Wpf;
using InTheHand.Net;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace DoutorIE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class toggle : Window
    {//rodrigorreis
        public static string version = "1.0.16";
        public static string HelloMessage = "Hello World xxxx";
        public string MacAddress { get { return HelloMessage; } }
        CefSharp.Wpf.ChromiumWebBrowser chromium;
        private Uri currentUri;
        public static toggle AppToggle;

        public static string subscribermsg = "rodrigorreis";
        public string subscriber { get; }

        public bool isBluetooth = false;
        public int toggleScreen = 0;// webview screen 

        public static string theScript = @"
                   function getLocation() {
                          if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition);
                          } else { 
                           alert('Geolocation is not supported by this browser.');
                          }
                        }

                    function showPosition(position)
                    {
                        alert('Latitude: ' + position.coords.latitude +
                        '<br>Longitude: ' + position.coords.longitude;
                    } ";


        public toggle(bool isWebView)
        {
            var userAgentParams = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.098 Safari/537.36";
            CefSettings settings = new CefSettings();
            settings.Locale = "pt-BR";
            settings.AcceptLanguageList = "pt-BR,en-US";
            settings.CachePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CEF";
            settings.UserAgent = userAgentParams;
            //Cef //Delete all cookies.
            //CefSharp.Cef.Initialize(settings);
            InitializeComponent();
            try
            {


                if (isWebView)
                {
                    isBluetooth = false;
                    toggleScreen = 0; // WebView screen 
                    ToggleScreen(true);
                }
                else
                {
                    toggleScreen = 1; // Bluetooth screen 
                    isBluetooth = true;
                    ToggleScreen(false);
                }
            }
            catch (Exception ex)
            {

                
            }
            // get geolocation for the native app
            //var detailsFromClass = GeoLocationDetails.GetMyGeoLocation();
            //MessageBox.Show(detailsFromClass, "Geo Location", MessageBoxButton.OK);


            chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser");
            cefSharpWebBrowser.Address = "https://drieonline.com/access.php?type=desktop";
            chromium.JavascriptObjectRepository.Register("boundAsync", new BoundObject(), true);
            chromium.JavascriptObjectRepository.Register("boundSubAsync", new BoundSubscribers(), true);


            var data = chromium.Address;
            btnAlert.Visibility = Visibility.Hidden;
            chromium.LoadingStateChanged += BrowserLoadingStateChanged;
            chromium.RequestHandler = new BrowserRequestHandler();
            chromium.LifeSpanHandler = new BrowserLifeSpanHandler();
            AppToggle = this;
            //AppWindow.WindowState = WindowState.Minimized;
            //  chromium.RequestHandler = new CustomRequestHandler("");
            chromium.DownloadHandler = new DownloadHandler(Bar);
            chromium.LoadError += OnLoadError;
        }

        public void HideIconsOfBrowser()
        {
            leftIcon.Visibility = Visibility.Hidden;
            rightIcon.Visibility = Visibility.Hidden;
            refreshIcon.Visibility = Visibility.Hidden;

        }
        public void ShowIconsOfBrowser()
        {
            leftIcon.Visibility = Visibility.Visible;
            rightIcon.Visibility = Visibility.Visible;
            refreshIcon.Visibility = Visibility.Visible;
        }


        private void OnLoadError(object sender, LoadErrorEventArgs args)
        {
            if (args.ErrorCode == CefSharp.CefErrorCode.InternetDisconnected)
            {
                // System.Threading.Thread.Sleep(2000);
                //MessageBox.Show("Please check your internet connection.");
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        MessageBox.Show(Application.Current.MainWindow, "Please check your internet connection.");
                    }));
                }
            }
            else
            {
                //if (Application.Current.Dispatcher.CheckAccess())
                //{
                //    MessageBox.Show(Application.Current.MainWindow,args.ErrorText);
                //}
                //else
                //{
                //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                //        MessageBox.Show(Application.Current.MainWindow, args.ErrorText);
                //    }));
                //}
            }
        }

        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            try
            {

                Application.Current.Dispatcher.Invoke(() =>
                {
                    //  AppToggle.Hide();
                    AppToggle.WindowState = WindowState.Maximized;
                    //AppToggle.chromium.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
                    //AppToggle.chromium.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
                });

                if (!e.IsLoading)
                {
                    if (e.Browser.MainFrame.Url == "https://drieonline.com/temp_integration/alert_js.html")
                    {
                        Application.Current.Dispatcher.Invoke(() => { AppToggle.btnAlert.Visibility = Visibility.Visible; });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() => { AppToggle.btnAlert.Visibility = Visibility.Hidden; });
                    }
                    if (e.Browser.MainFrame.Url == "https://www.drieonline.com/logout.php")
                    {
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        var scriptTask = e.Browser.MainFrame.EvaluateScriptAsync(theScript);
                        scriptTask.ContinueWith(u =>
                        {
                            if (u.Result.Success && u.Result.Result != null)
                            {
                            }
                        });

                    });
                    Application.Current.Dispatcher.Invoke(() => { AppToggle.loadingGif.Visibility = Visibility.Hidden; });
                    Application.Current.Dispatcher.Invoke(() => { AppToggle.Show(); });


                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => { AppToggle.btnAlert.Visibility = Visibility.Hidden; });
                    Application.Current.Dispatcher.Invoke(() => { AppToggle.loadingGif.Visibility = Visibility.Visible; });
                }
            }
            catch
            {
            }
        }
        //private static void Browser2LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        //{
        //    if (!e.IsLoading)
        //    {
        //        Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif2.Visibility = Visibility.Hidden; });
        //    }
        //    else
        //    {
        //       Application.Current.Dispatcher.Invoke(() => { AppWindow.loadingGif2.Visibility = Visibility.Visible; });
        //    }
        //}
        private async void Alert_Click(object sender, RoutedEventArgs e)
        {
            //var browser = new ChromiumWebBrowser("https://drieonline.com/temp_integration/alert_js.html");

            if (chromium.CanExecuteJavascriptInMainFrame)
            {
                chromium.GetMainFrame().ExecuteJavaScriptAsync(@"someJQFunction();");
                //JavascriptResponse response = await browser.EvaluateScriptAsync("d");
                //if (response.Result != null)
                //{
                //    //MessageBox.Show(response.Result.ToString, "JS Result");
                //}
            }
        }

        /// <summary>
        /// Generate alert from the browser
        /// </summary>
        public void alertMessage()
        {
            //chromium.EvaluateScriptAsync
            chromium.GetMainFrame().ExecuteJavaScriptAsync(@"someJQFunction();");
        }



        public static string GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    HelloMessage = Convert.ToString(nic.GetPhysicalAddress());
                    return nic.GetPhysicalAddress().ToString();
                }
            }
            return null;
        }

        private void btnbluetooth_Click(object sender, RoutedEventArgs e)
        {
            var bluetoothpage = new BluetoothWindow();
            bluetoothpage.Show();
            this.Close();
        }

        private void mytab2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //mytab2.Content = Application.LoadComponent(new Uri("UserControl1.xaml", UriKind.Relative));
        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

        //    switch (tabItem)
        //    {
        //        case "Home2":
        //            if (chromium != null)
        //            {

        //                chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser2");
        //                chromium.LoadingStateChanged += Browser2LoadingStateChanged;
        //                chromium.LoadError += OnLoadError;
        //            }
        //            break;

        //        case "Bluetooth":

        //            break;

        //        case "Home":

        //           if(chromium !=null)
        //            {
        //             chromium = (ChromiumWebBrowser)this.FindName("cefSharpWebBrowser");

        //            }
        //            break;

        //        default:
        //            return;
        //    }
        //}

        protected async override void OnClosing(CancelEventArgs e)
        {

            MessageBoxResult result = CustomMessageBox.ShowYesNo(
                "Deseja realmente sair do aplicativo Doutor-IE?",
                "Doutor-IE",
                "Sim",
                "Não",
                MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.Yes)
            {
                e.Cancel = true;
                //MessageBox.Show("Encerrando o aplicativo, por gentileza aguarde...", "Message Box");
                try
                {
                    chromium.ExecuteScriptAsync("location.href = 'logout.php';");
                    await cefSharpWebBrowser.GetSourceAsync().ContinueWith(taskHtml =>
                      {
                          var html = taskHtml.Result;
                          cefSharpWebBrowser.GetCookieManager().DeleteCookies();
                          System.Threading.Thread.Sleep(2000);
                          this.Dispatcher.Invoke(() =>
                          {
                              System.Environment.Exit(0);
                          });
                      });
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Encerrando o aplicativo, por gentileza aguarde...", "Message Box");
                }
            }

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }



        private void menu_MouseDown(object sender, MouseEventArgs e)
        {
            if (lblMenuContent.IsVisible)
            {
                lblMenuContent.Visibility = Visibility.Hidden;
            }
            else
            {
                lblMenuContent.Visibility = Visibility.Visible;
            }
        }



        //public string Encode(string input, byte[] key)
        //{
        //    HMACSHA1 myhmacsha1 = new HMACSHA1(key);
        //    byte[] byteArray = Encoding.ASCII.GetBytes(input);
        //    MemoryStream stream = new MemoryStream(byteArray);
        //    return myhmacsha1.ComputeHash(stream).Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);
        //}

        //private string CreateToken(string message, string secret)
        //{
        //    secret = secret ?? "";
        //    var encoding = new System.Text.ASCIIEncoding();
        //    byte[] keyByte = encoding.GetBytes(secret);
        //    byte[] messageBytes = encoding.GetBytes(message);
        //    using (var hmacsha1 = new HMACSHA1(keyByte))
        //    {
        //        byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
        //        return Convert.ToBase64String(hashmessage);
        //    }
        //}

        private void Browser_PreviewMouseRightButton(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void Button_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void BackButtonClick(object sender, EventArgs e)
        {
            chromium.Back();
        }

        private void ForwardButtonClick(object sender, EventArgs e)
        {
            chromium.Forward();
        }
        private void ReloadButtonClick(object sender, EventArgs e)
        {
            chromium.Reload();
        }

        private void OpenContactMenu(object sender, MouseButtonEventArgs e)
        {

            Image image = sender as Image;
            ContextMenu contextMenu = image.ContextMenu;
            contextMenu.PlacementTarget = image;
            contextMenu.IsOpen = true;
            e.Handled = true;

        }
        public void ToggleButton_click(object sender, MouseButtonEventArgs e)
        {
            Image image = (Image)sender;

            if(image.Name == "toggleButtonwebview")
            {
                ToggleScreen(true);
            }
            else
            {
                ToggleScreen(false);
            }
            //if (toggleScreen == 0) // web to blue screen 
            //{
            //    toggleScreen = 1;
            //    ToggleScreen(false);
            //}
            //else
            //{
            //    toggleScreen = 0;
            //    ToggleScreen(true);
            //}
        }
        public void ToggleScreen(bool isWebView)
        {
            try
            {


                string appDir = Path.GetDirectoryName(
                 Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (isWebView)
                {
                    isBluetooth = false;
                    srcWebView.Visibility = Visibility.Visible;
                    srcBluetooth.Visibility = Visibility.Hidden;
                    ShowIconsOfBrowser();

                    //selected webview image Bind
                    var imagesPath = Path.Combine(appDir, "darkwebview.png");
                    toggleButtonwebview.Source = new BitmapImage(new Uri(imagesPath));
                    toggleButtonwebview.IsEnabled = false;

                    //non selected bluetooth image bind
                    var imagesbluetoothPath = Path.Combine(appDir, "whitebluetooth.png");
                    //var imagesbluetoothPath = Path.Combine(appDir, "toggleBluetooth.png");
                    toggleButtonbluetooth.Source = new BitmapImage(new Uri(imagesbluetoothPath));
                    toggleButtonbluetooth.IsEnabled = true;
                }
                else
                {
                    
                    isBluetooth = true;
                    srcBluetooth.Visibility = Visibility.Visible;
                    srcWebView.Visibility = Visibility.Hidden;
                    HideIconsOfBrowser();

                    //selected bluetooth image Bind
                    var imagesbluetoothPath = Path.Combine(appDir, "darkbluetooth.png");
                    toggleButtonbluetooth.Source = new BitmapImage(new Uri(imagesbluetoothPath));
                    toggleButtonbluetooth.IsEnabled = false;

                    //non selected bluetooth image bind

                    var imageswebviewPath = Path.Combine(appDir, "whitewebview.png");
                    toggleButtonwebview.Source = new BitmapImage(new Uri(imageswebviewPath));
                    toggleButtonwebview.IsEnabled = true;
                    getListOfDevice();

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        private void Sobre_Click(object sender, RoutedEventArgs e)
        {
            chromium.ExecuteScriptAsync("getLocation()");

            MessageBox.Show("Doutor-IE Tecnologia Automotiva"
                 + Environment.NewLine +
                "Copyright © 2020 Todos os direitos reservados"
                 + Environment.NewLine + " "
                + Environment.NewLine +
                "Telefone: (48) 3238-0010"
                 + Environment.NewLine +
                 "Site: https://www.doutorie.com.br"
                 + Environment.NewLine + " "
                 + Environment.NewLine +
                 "Versão   " + version + " "
               , "Sobre");


        }

        private void BacktomainWindow_Click(object sender, RoutedEventArgs e)
        {
            StartPage startPage = new StartPage();
            startPage.Show();
            AppToggle.Hide();
        }

        public void getListOfDevice()
        {
            List<BluetoothModal> items = new List<BluetoothModal>();
            BluetoothClient client = new BluetoothClient();
            BluetoothDeviceInfo[] devices = client.DiscoverDevices();
            foreach (BluetoothDeviceInfo d in devices)
            {
                items.Add(new BluetoothModal
                {
                    bluetoothid = d.DeviceAddress,
                    name = d.DeviceName
                });

            }
            lstbluetoothdevice.DisplayMemberPath = "name";
            lstbluetoothdevice.SelectedValue = "bluetoothid";
            lstbluetoothdevice.ItemsSource = items;
        }
    }

    public class BoundObject
    {

        public string getMacAddress(string msg)
        {
            var mac = GetMacAddress();
            return mac;
        }

        public static string GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress().ToString();
                }
            }
            return null;
        }

        public string getSignature(string msg)
        {
            var se = GetSignature();
            return se;
        }

        public static string GetSignature()
        {
            string signature = null;
            if (!string.IsNullOrEmpty(GetSetComanDetails.Signature))
            {
                var macAddress = GetMacAddress();
                if (!string.IsNullOrWhiteSpace(macAddress))
                {
                    signature = GetSetComanDetails.Signature + "," + macAddress + ",,,," + toggle.version;
                }
                else
                {
                    signature = GetSetComanDetails.Signature + ",,,,," + toggle.version;
                }

            }
            return signature;
        }
    }



    // bind the subscriber and created the signature 
    public class BoundSubscribers
    {
        public void getSubscribers(string subscriberName, string date)
        {
            //API key code
            byte[] bytes = new byte[16];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(bytes);
            var key = "iiwsivk8ab624sszj6g85y9304ee9wkh";
            SiteVariable.Key = key;
            var ccDate = DateTime.Now.ToString("dd/MM/yyyy");
            var currentDate = ccDate.Replace("-", "/");
            var secretkey = subscriberName + "_" + date;
            // get signature
            var signature = GenerateSignature(secretkey, key); // S = MAC(K, ID, D) where S is signature, K is key, ID is subscriber and D is current data
            // set values
            GetSetComanDetails.Signature = signature;
        }
        public string GenerateSignature(string secretKey, string signatureString)
        {
            var enc = Encoding.ASCII;
            HMACSHA1 hmac = new HMACSHA1(enc.GetBytes(secretKey));
            hmac.Initialize();
            byte[] buffer = enc.GetBytes(signatureString);
            return BitConverter.ToString(hmac.ComputeHash(buffer)).Replace("-", "").ToLower();
        }

    }
    public static class GetSetComanDetails
    {
        public static string Signature { get; set; }
    }

    public class DownloadHandler : IDownloadHandler
    {
        private ProgressBar _bar;
        public DownloadHandler(ProgressBar bar)
        {
            _bar = bar;

        }
        public void OnBeforeDownload(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
        {
            if (!callback.IsDisposed)
            {
                using (callback)
                {
                    string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    //string pathDownload = Path.Combine(pathUser, "Downloads\\");
                    //System.IO.FileInfo file = new System.IO.FileInfo(downloadItem.FullPath);
                    if (browser.MainFrame.Url == "https://drieonline.com/temp_versao.php")
                    {
                        if (downloadItem.MimeType == "application/csv")
                        {
                            callback.Continue(pathUser + "\\Downloads\\" + downloadItem.SuggestedFileName, showDialog: false);
                        }
                        else
                        {
                            callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
                        }
                    }
                    else
                    {
                        callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
                    }


                }
            }
        }

        public void OnDownloadUpdated(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
        {

            _bar.Dispatcher.Invoke(new Action(() =>
            {
                Debug.Print("{0}/{1} bytes", downloadItem.ReceivedBytes, downloadItem.TotalBytes);
                _bar.Visibility = Visibility.Hidden;
                //_bar.Maximum = downloadItem.TotalBytes;
                _bar.Value = downloadItem.PercentComplete;
                if (downloadItem.IsComplete)
                {
                    System.Threading.Thread.Sleep(2000);
                    _bar.Visibility = Visibility.Hidden;
                    if (browser.MainFrame.Url == "https://drieonline.com/temp_versao.php")
                    {
                        if (downloadItem.MimeType == "application/csv")
                        {

                            _bar.Visibility = Visibility.Hidden;
                            //MessageBox.Show("File download sccessfully at location: " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\");
                            CustomMessageBox.Show(
                               "Download realizado com sucesso! at " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\",
                               "Doutor-IE");

                        }
                        else
                        {
                            _bar.Visibility = Visibility.Hidden;
                            CustomMessageBox.Show(
                                  "Download realizado com sucesso!",
                                  "Doutor-IE");
                            //MessageBox.Show("File download sccessfully.");
                        }
                    }
                    else
                    {
                        _bar.Visibility = Visibility.Hidden;
                        CustomMessageBox.Show(
                                  "Download realizado com sucesso!",
                                  "Doutor-IE");
                        //MessageBox.Show("File download sccessfully.");
                    }
                }
            }));
        }
    }



    public class CustomResourceRequestHandler : ResourceRequestHandler
    {
        private string _signature;
        public CustomResourceRequestHandler(string signature)
        {
            _signature = GetSetComanDetails.Signature;
        }

        protected override CefReturnValue OnBeforeResourceLoad(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
        {
            if (request.Url.Contains("https://drieonline.com"))
            {
                if (!string.IsNullOrEmpty(GetSetComanDetails.Signature))
                {
                    var headers = request.Headers;
                    headers["s"] = GetSetComanDetails.Signature;
                    request.Headers = headers;
                    return CefReturnValue.Continue;
                }
                else return base.OnBeforeResourceLoad(chromiumWebBrowser, browser, frame, request, callback);
            }
            else return base.OnBeforeResourceLoad(chromiumWebBrowser, browser, frame, request, callback);
        }

    }
    public class CustomRequestHandler : RequestHandler
    {
        private string _signature;
        public CustomRequestHandler(string signature)
        {
            _signature = GetSetComanDetails.Signature;
        }

        protected override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
        {
            if (request.Url.Contains("https://drieonline.com") && request.Url.Contains(".php"))
            {
                if (!string.IsNullOrEmpty(GetSetComanDetails.Signature)) return new CustomResourceRequestHandler(GetSetComanDetails.Signature);
                else return base.GetResourceRequestHandler(chromiumWebBrowser, browser, frame, request, isNavigation, isDownload, requestInitiator, ref disableDefaultHandling);
            }
            else return base.GetResourceRequestHandler(chromiumWebBrowser, browser, frame, request, isNavigation, isDownload, requestInitiator, ref disableDefaultHandling);
        }
    }

    public class SubtractionConverter : System.Windows.Data.IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Default to 0. You may want to handle divide by zero 
            // and other issues differently than this.
            double result = 0;

            // Not the best code ever, but you get the idea.
            if (value != null)
            {
                try
                {
                    double numerator = (double)value;


                    result = numerator-80 ;
                }
                catch (Exception e)
                {
                    // TODO: Handle casting exceptions.
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class BrowserRequestHandler : RequestHandler
    {


        protected override bool OnBeforeBrowse(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool userGesture, bool isRedirect)
        {
            if (request.Url.StartsWith("https://doutorie.app.vindi.com.br"))
            {
                // Open Url in Default browser 
                System.Diagnostics.Process.Start(request.Url);
                return true;
            }
            else if (request.Url.StartsWith("https://www.drieonline.com/cliente"))
            {
                // Open Url in Default browser 
                System.Diagnostics.Process.Start(request.Url);
                return true;
            }
            else
            {
                // Url except Google open in CefSharp's Chromium browser
                return false;
            }
        }



    }
    public class BrowserLifeSpanHandler : ILifeSpanHandler
    {
        public bool DoClose(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            return false;
        }

        public void OnAfterCreated(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {

        }

        public void OnBeforeClose(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {

        }


        bool ILifeSpanHandler.OnBeforePopup(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
            if (targetUrl.StartsWith("https://doutorie.app.vindi.com.br") || targetUrl.StartsWith("https://www.drieonline.com/cliente"))
            {
                browser.MainFrame.LoadUrl(targetUrl);
                newBrowser = null;
                return true;
            }
            else
            {
                newBrowser = null;
                return false;
            }
        }
    }
    public class BluetoothModal
    {
        public BluetoothAddress bluetoothid { get; set; }
        public string name { get; set; }
    }
}
﻿namespace DoutorIE
{
    internal static class Util
    {
        internal static string TryAddKeyboardAccellerator(this string input)
        {
            const string accellerator = "_"; // This is the default WPF accellerator symbol - used to be & in WinForms

            // If it already contains an accellerator, do nothing
            if (input.Contains(accellerator)) return input;

            return accellerator + input;
        }
    }
}

﻿namespace DoutorIE
{
    class SiteVariable
    {
        private static string _key = "";

        public static string Key
        {
            get { return _key; }
            set { _key = value; }
        }
    }
}
